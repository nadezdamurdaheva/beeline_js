const objectOfGroups = {}; 

window.addEventListener('load',creatingObjectOfLectures);

function creatingObjectOfLectures() {
  const cardContainers = document.querySelectorAll('.card-container');

  for (let cardContainer of cardContainers) {
    if (cardContainer.parentElement.id != 'all-lect1') {
      const cardElems = cardContainer.querySelectorAll('.card');
      for (let cardElem of cardElems) {
        const keyGroup = cardElem.dataset.group;
        if (!objectOfGroups.hasOwnProperty(keyGroup)) {
          objectOfGroups[keyGroup] = [];
        }
        const objectOneLecture = {};
        const imageLectureUrl = getComputedStyle(cardElem.childNodes[1]).backgroundImage;
        objectOneLecture.title = cardElem.querySelector('.card__text1').textContent;
        objectOneLecture.description =cardElem.querySelector('.card__text2').textContent;
        objectOneLecture.date = cardElem.querySelector('.card__date').textContent;
        objectOneLecture.image = imageLectureUrl.slice(imageLectureUrl.lastIndexOf('/'),-2);
        objectOneLecture.label = cardElem.querySelector('.card-image-text').textContent;
        objectOfGroups[keyGroup].push(objectOneLecture);
      }
    }
  }
  console.log(objectOfGroups);
}
