let cardText_1 = document.querySelectorAll('#base-lecture-container .card__text1');
let cardText_2 = document.querySelectorAll('#base-lecture-container .card__text2');

for(let i = 0; i < cardText_1.length; i++) {
    cardText_1[i].textContent = cardText_1[i].textContent.toUpperCase();
}

for(let i = 0; i < cardText_2.length; i++) {
    if(cardText_2[i].textContent.length > 20 ) {
        cardText_2[i].textContent = cardText_2[i].textContent.slice(0,20) + '...';
    }
}