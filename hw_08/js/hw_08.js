window.onload = function () {
  creatingCards();
};

/* фильтр html */
let htmlButton = document.querySelector(".html-button");
htmlButton.addEventListener("click", (elem) => {
  creatingCards("html", elem.currentTarget);
});

/* фильтр css */
let cssButton = document.querySelector(".css-button");
cssButton.addEventListener("click", (elem) => {
  creatingCards("css", elem.currentTarget);
});

/* фильтр js */
let jsButton = document.querySelector(".js-button");
jsButton.addEventListener("click", (elem) => {
  creatingCards("js", elem.currentTarget);
});

/* фильтр показать все лекции */
let allLecturesButton = document.querySelector(".all-lectures-button");
allLecturesButton.addEventListener("click", (elem) => {
  creatingCards("", elem.currentTarget);
});

/* перемещаться назад по всем card-container */
let prevBtnAllLecture = document
  .getElementById("all-lecture-container")
  .querySelector(".prev1");
prevBtnAllLecture.addEventListener("click", function () {
  let lectureContainer = document
    .getElementById("all-lecture-container")
    .querySelector(".lecture");
  let cardContainers = lectureContainer.querySelectorAll(".card-container");

  if (cardContainers.length < 2) return;
  for (let i = 0; i <= cardContainers.length - 1; i++) {
    if (!cardContainers[i].classList.contains("card-container-not-show")) {
      cardContainers[i].classList.toggle("card-container-not-show");
      if (i == cardContainers.length - 1) {
        cardContainers[0].classList.toggle("card-container-not-show");
      } else {
        cardContainers[i + 1].classList.toggle("card-container-not-show");
      }
      break;
    }
  }
});

/* перемещаться вперед по всем card-container */
let nextBtnAllLecture = document
  .getElementById("all-lecture-container")
  .querySelector(".next1");
nextBtnAllLecture.addEventListener("click", function () {
  let lectureContainer = document
    .getElementById("all-lecture-container")
    .querySelector(".lecture");
  let cardContainers = lectureContainer.querySelectorAll(".card-container");

  if (cardContainers.length < 2) return;
  for (let i = 0; i <= cardContainers.length - 1; i++) {
    if (!cardContainers[i].classList.contains("card-container-not-show")) {
      cardContainers[i].classList.toggle("card-container-not-show");
      if (i == cardContainers.length - 1) {
        cardContainers[0].classList.toggle("card-container-not-show");
      } else {
        cardContainers[i + 1].classList.toggle("card-container-not-show");
      }
      break;
    }
  }
});

function creatingCards(dataGroup, nameButton) {
  let numOfLectures = 0;
  let cardsAllMassive =
    []; /* массив карточек, в одной карточке вся информация по одной лекции */
  deleteCardContainers();
  creatingCardsAllMassive(numOfLectures, cardsAllMassive, dataGroup);
  addCardContainers(cardsAllMassive);
  addCarsInCardContainers(cardsAllMassive);
  changeStyleFilter(nameButton);
}

function creatingCardsAllMassive(numOfLectures, cardsAllMassive, dataGroup) {
  let cardContainers = document.querySelectorAll(".card-container");

  for (let i of cardContainers) {
    let cardElems = i.querySelectorAll(".card");
    for (let j = 0; j <= cardElems.length - 1; j++) {
      numOfLectures++;
      /* если совпадает по data-group, добавляется элемент в массив cardsAllMassive,  */
      if (cardElems[j].dataset.group == dataGroup || !dataGroup) {
        let card = document.createElement("div");
        card.classList.add("card");
        /* добавить в каждый card содержимое карточки лекции*/
        let str = cardElems[j].innerHTML.toString();
        card.insertAdjacentHTML("beforeend", str);
        cardsAllMassive.push(card);
      }
    }
  }
  /* здесь записываем общее кол-во лекций */
  let numLectures = document.querySelector(".num-lectures");
  numLectures.textContent = numOfLectures + " лекций";
}

/* добавление карточек во все блоки с классом .card-container */
function addCardContainers(cardsAllMassive) {
  for (let m = 0; m <= cardsAllMassive.length - 1; m++) {
    let allLectureContainer = document
      .getElementById("all-lecture-container")
      .querySelector(".lecture");
    let hasCardContainer = allLectureContainer.querySelector(".card-container");
    /* добавляем новый card-container:
    1) если его нет;
    2) если образовалось больше 4х элементов в card-container.
    Если дошли до последнего элемента массива, то новый card-container не добавляем */
    if (
      !hasCardContainer ||
      ((m + 1) % 4 == 0 && m != cardsAllMassive.length - 1)
    ) {
      let divAllLecture = document.createElement("div");
      divAllLecture.classList.add("card-container");
      allLectureContainer.append(divAllLecture);
    }
  }
}

function addCarsInCardContainers(cardsAllMassive) {
  let divsAllLecture = document
    .getElementById("all-lecture-container")
    .querySelectorAll(".card-container");
  let n = 0;
  while (n <= divsAllLecture.length - 1) {
    for (let m = 0; m <= cardsAllMassive.length - 1; m++) {
      divsAllLecture[n].append(cardsAllMassive[m]);
      /* скрыть все последующие блоки card-container (кроме первого) */
      if (m > 3) {
        divsAllLecture[n].classList.add("card-container-not-show");
      }
      /* как только в блоке card-container добавилось 4 элемента, 
  добавлять последующие элементы в следующий блок card-container */
      if ((m + 1) % 4 == 0) {
        n++;
      }
    }
    n++;
  }
}

function deleteCardContainers() {
  let lectureContainer = document
    .getElementById("all-lecture-container")
    .querySelector(".lecture");
  let cardContainers = lectureContainer.querySelectorAll(".card-container");
  for (let i of cardContainers) {
    lectureContainer.removeChild(i);
  }
}

function changeStyleFilter(nameButton) {
  let massiveFilters = [];
  massiveFilters.push(htmlButton);
  massiveFilters.push(cssButton);
  massiveFilters.push(jsButton);
  massiveFilters.push(allLecturesButton);
  for (let item of massiveFilters) {
    item.classList.remove("lecture__link-all-active-filter");
  }
  if (nameButton) {
    nameButton.classList.add("lecture__link-all-active-filter");
  }
}
